# Uniswap Widget

Uniswap widget is a web widget which can be used to trade ERC-20 tokens with uniswap protocol. It is easy to setup on any web page which has access to Metamask extension.

## Preliminary Requirements

### 1. Get **UNISWAPDEX_CLIENT_SECRET** key

- Contact admin@uniswapdex.com to get a `UNISWAPDEX_CLIENT_SECRET` key

### 2. Register Google reCAPTCHA v3 service

- Go to https://www.google.com/recaptcha/intro/v3.html
- Sign into google reCAPTCHA service and register a new site
- Get a `client_key` and `client_secret`

## Authentication Server Setup

- Clone `auth-server` repo on your remote server `git clone https://gitlab.com/shardus/uniswap/auth-server.git`
- cd into `auth-server` directory. `cd auth-server`
- Replace `UNISWAPDEX_CLIENT_SECRET` value in `.env` file
- Replace `RECAPTCHA_SECRET_KEY` value in `.env` file
- Change `port` number in `config.js` to your desired value
- Install dependencies `npm install`
- Start server by `npm start`

## Client Setup

1. Import css files into `header` section your HTML file.

   ```
   <link
     rel="stylesheet"
     href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
     integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
     crossorigin="anonymous"
   />
   <script
     src="https://kit.fontawesome.com/779ce9544f.js"
     crossorigin="anonymous"
   ></script>

   <link rel="stylesheet" href="uniswap-widget.css" type="text/css" />
   ```

2. Add a `div` with id of `uniswap-convert-section` into body of your HTML file.

   ```
   <div id="uniswap-convert-section"></div>
   ```

3. Import required JS files (jQuery, Popper, Bootstrap, BigNumber, widget style) before the end of your `body` section.

   ```
    <script
      src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
      integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
      integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/gh/ethereum/web3.js@1.0.0-beta.36/dist/web3.min.js"
      integrity="sha256-nWBTbvxhJgjslRyuAKJHK+XcZPlCnmIAAMixz6EefVk="
      crossorigin="anonymous"
    ></script>
    <script src="bignumber.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=RECAPTCHA_CLIENT_KEY"></script>

   ```

4. Replace `RECAPTCHA_CLIENT_KEY` value in `src` attribute of above script tag
5. Add uniswap widget library
   ```
   <script src="crypto-bundle.js"></script>
   <script type="text/javascript" src="uniswap-widget.js"></script>
   ```
6. Add a script tag to configure uniswap widget and initialise it.

   ```
   <script>
   // change mainToken field as needed
      let mainConfig = {
        mainToken: {
          symbol: 'BAT',
          tokenAddress: '0x0D8775F648430679A709E98d2b0Cb6250d2887EF',
          exchangeAddress: '0x2e642b8d59b45a1d8c5aef716a84ff44ea665914'
        },
        mainServerUrl: 'https://dev.uniswapdex.com:8889'
      }

      let widgetConfig = {
        mainToken: mainConfig.mainToken,
        tokenListUrl: 'https://beta.shardus.com/assets/js/tokenDB.json',
        summaryUrl: `${mainConfig.mainServerUrl}/api/summary?tokenAddress=${mainConfig.mainToken.tokenAddress}`,
        chartUrl: `https://uniswapdex.com/?token=${mainConfig.mainToken.tokenAddress}`,
        logoUrl: `${
          mainConfig.mainServerUrl
        }/static/${mainConfig.mainToken.tokenAddress.toLowerCase()}.png`,
        authServerUrl: AUTH_SERVER_URL,
        mainConfig,
        widgetTitle: '',
        recaptchaClientKey: RECAPTCHA_CLIENT_KEY
      }

      UniswapConvertWidget(widgetConfig)
   </script>
   ```

7. Update `mainToken` field of `mainConfig` obj to any uniswap listed ERC-20. Token address and exchange address of all uniswap listed tokens can be find on [UniswapDEX](https://uniswapdex.com). Replace `authServerUrl` and `recaptchaClientKey` to correct values
